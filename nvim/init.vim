set nu rnu
let g:ale_lua_luacheck_options='-g -u'
set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab

set splitbelow
set splitright

" Remap iaeo to hjkl. (QGMLWY)
"noremap i h
"noremap h i

"noremap a j
"noremap j a

"noremap e k
"noremap k e

"noremap o l
"noremap l o

" Put these lines at the very end of your vimrc file.

" Load all plugins now.
" Plugins need to be added to runtimepath before helptags can be generated.
packloadall
" Load all of the helptags now, after plugins have been loaded.
" All messages and errors will be ignored.
silent! helptags ALL
